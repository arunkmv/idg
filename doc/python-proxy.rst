Python IDG Module
=================


.. automodule:: idg
   :members:

Submodules
==========

.. toctree::
   :maxdepth: 2
   
   python-idg-cpu
   python-idg-cuda
   python-idg-hybrid-cuda
   python-idg-opencl
   

Classes
=======

.. autoclass:: idg.Proxy.Proxy
   :members:


